﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using WpfQuestionnaire.Custom;

namespace WpfQuestionnaire.Demo
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : INotifyPropertyChanged
    {
        public MainWindow()
        {
            InitializeComponent();
            Loaded += OnLoaded;
        }

        private void OnLoaded(object sender, RoutedEventArgs e)
        {
            MyQuestionnaire.QuestionnaireSubmitted += OnSubmitted;
            QuestionTwoOneOptions = new List<string>
            {
                "Apple",
                "Orange",
                "Pear"
            };
        }

        private List<string> _questionTowOneOptions;

        public event PropertyChangedEventHandler PropertyChanged;


        protected virtual void OnPropertyChanged(string property)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(property));
        }

        private void OnSubmitted(object sender, QuestionnaireSubmittedEventArgs e)
        {
            foreach (var a in e.AllAnswers)
            {
                   Debug.WriteLine(a.Answer);
            }
        }

        public List<string> QuestionTwoOneOptions
        {
            get { return _questionTowOneOptions; }
            set
            {
                _questionTowOneOptions = value;
                OnPropertyChanged(nameof(QuestionTwoOneOptions));
            }
        }

    }
}
